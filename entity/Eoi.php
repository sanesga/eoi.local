<?php
/**
 * Created by PhpStorm.
 * User: Sandra
 * Date: 17/12/2018
 * Time: 22:28
 */
require_once __DIR__ . '/../database/IEntity.php';

class Eoi implements IEntity
{
    /**
     * @var int
     */
    private $codigo;
    /**
     * @var int
     */
    private $cod_provincia;
    /**
     * @var string
     */
    private $nom_provincia;
    /**
     * @var int
     */
    private $cod_municipio;
    /**
     * @var string
     */
    private $nom_municipio;

    /**
     * Eoi constructor.
     * @param int $codigo
     * @param int $cod_provincia
     * @param string $nom_provincia
     * @param int $cod_municipio
     * @param string $nom_municipio
     */
    public function __construct(int $codigo=0, int $cod_provincia=0, string $nom_provincia='', int $cod_municipio=0, string $nom_municipio='')
    {
        $this->codigo = $codigo;
        $this->cod_provincia = $cod_provincia;
        $this->nom_provincia = $nom_provincia;
        $this->cod_municipio = $cod_municipio;
        $this->nom_municipio = $nom_municipio;
    }

    public function toArray(): array{
        return[
            'codigo'=>$this->getCodigo(),
            'cod_provincia'=>$this->getCodProvincia(),
            'nom_provincia'=>$this->getNomProvincia(),
            'cod_municipio'=>$this->getCodMunicipio(),
            'nom_municipio'=>$this->getNomMunicipio()
        ];
    }

    /**
     * @return int
     */
    public function getCodigo(): int
    {
        return $this->codigo;
    }

    /**
     * @param int $codigo
     * @return Eoi
     */
    public function setCodigo(int $codigo): Eoi
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return int
     */
    public function getCodProvincia(): int
    {
        return $this->cod_provincia;
    }

    /**
     * @param int $cod_provincia
     * @return Eoi
     */
    public function setCodProvincia(int $cod_provincia): Eoi
    {
        $this->cod_provincia = $cod_provincia;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomProvincia(): string
    {
        return $this->nom_provincia;
    }

    /**
     * @param string $nom_provincia
     * @return Eoi
     */
    public function setNomProvincia(string $nom_provincia): Eoi
    {
        $this->nom_provincia = $nom_provincia;
        return $this;
    }

    /**
     * @return int
     */
    public function getCodMunicipio(): int
    {
        return $this->cod_municipio;
    }

    /**
     * @param int $cod_municipio
     * @return Eoi
     */
    public function setCodMunicipio(int $cod_municipio): Eoi
    {
        $this->cod_municipio = $cod_municipio;
        return $this;
    }

    /**
     * @return string
     */
    public function getNomMunicipio(): string
    {
        return $this->nom_municipio;
    }

    /**
     * @param string $nom_municipio
     * @return Eoi
     */
    public function setNomMunicipio(string $nom_municipio): Eoi
    {
        $this->nom_municipio = $nom_municipio;
        return $this;
    }


}