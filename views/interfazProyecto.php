<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>ESCUELAS OFICIALES IDIOMAS -Comunidad Valenciana-</title>

        <style>
            .estil_jumbotron{
                background-image: url(../images/fondo.jpg);
                color:white;
                text-align: center;
                background-size: contain;
            }
            
        </style>
    </head>
    <body>
         <div class="container">
        <header>
        
            <div class="jumbotron estil_jumbotron">
                <div class="mx-auto" style="width:700px;">
                    <h1 class="display-3">Escoles Oficials Idiomes</h1> 
                    <h3>-Comunitat Valenciana-</h3> 
                  
                </div> 
          
        </div>

        </header>
       
            <table class="table table-hover">
                <tr class="table-primary">
                    <th scope="col">Código</th>
                    <th scope="col">Código Provincia</th>
                    <th scope="col">Nombre Provincia</th>
                    <th scope="col">Código Municipio</th>
                    <th scope="col">Nombre Municipio</th>
                </tr>

                <tbody>
                <?php foreach($eois ?? [] as $eoi) : ?>
                    <tr class="table-light">
                        <th scope="row"><?= $eoi->getCodigo()?></th>
                        <td><?= $eoi->getCodProvincia()?></td>
                        <td><?= $eoi->getNomProvincia()?></td>
                        <td><?= $eoi->getCodMunicipio()?></td>
                        <td><?= $eoi->getNomMunicipio()?></td>
                    </tr>
                <?php  endforeach;?>
                </tbody>
            </table>

        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
    <footer></footer>
</html>