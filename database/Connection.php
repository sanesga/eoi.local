<?php
/**
 * Created by PhpStorm.
 * User: Sandra
 * Date: 07/12/2018
 * Time: 9:01
 */

class Connection
{
    /**
     * @return PDO
     */
    public static function make()
    {
        $config=App::get('config')['database'];
        try {
            $connection = new PDO(
                $config['connection'] . ';dbname=' . $config['name'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        }catch(PDOException $PDOException){
            die($PDOException->getMessage());
        }
        return $connection;
    }

}