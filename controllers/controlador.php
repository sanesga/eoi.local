<?php
/**
 * Created by PhpStorm.
 * User: Sandra
 * Date: 17/12/2018
 * Time: 21:49
 */
require_once __DIR__ . '/../repository/EoisRepository.php';

$config = require_once __DIR__ . '/../app/config.php';
App::bind('config', $config);
$eoisRepository = new EoisRepository();

$eois = $eoisRepository->findAll();
