<?php

return[
    'database'=>[
        'name' =>'escuelas_oficiales_idiomas',
        'username'=>'root',
        'password'=>'',
        'connection'=>'mysql:host=eoi.local',
        'options'=>[
            PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES utf8",
            PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT=>true
        ]
    ]
];